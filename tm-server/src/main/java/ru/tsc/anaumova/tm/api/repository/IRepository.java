package ru.tsc.anaumova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.model.AbstractModel;

import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void update(@NotNull M model);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void clear();

    long getCount();

    boolean existsById(@NotNull String id);

}